module.exports = function() {
	$.gulp.task("scripts", function(done) {
		return $.gulp.src("./app/js/main.js")
			.pipe($.gp.if($.minificate, $.gp.uglify()))
			.pipe($.gulp.dest("./build/js/"))
			.on("end", $.bs.reload, function() {
				done();
			});
	});
	$.gulp.task("bundle", function(done) {
		return $.gulp.src([
				"./node_modules/popper.js/dist/umd/popper.min.js",
				"./node_modules/bootstrap/dist/js/bootstrap.min.js",
				"./node_modules/jquery-mask-plugin/dist/jquery.mask.min.js",
				"./node_modules/jquery-validation/dist/jquery.validate.min.js",
				"./node_modules/jquery-mousewheel/jquery.mousewheel.js",
				"./node_modules/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js",
				// "./node_modules/object-fit-images/dist/ofi.min.js",
				"./node_modules/owl.carousel/dist/owl.carousel.min.js",
				"./node_modules/svg4everybody/dist/svg4everybody.min.js",
				"./node_modules/typed.js/lib/typed.min.js"
			])
			.pipe($.gp.concat('vendor.min.js'))
			.pipe($.gulp.dest("./build/js/"))
			.on("end", function() {
				done()
			});
	});
};