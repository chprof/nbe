module.exports = function () {
	$.gulp.task('bowerFonts', function() {
		return $.gulp.src($.mainFiles('**/*.{woff,woff2}'))
			.pipe($.gulp.dest('./app/fonts/'))
	});
};