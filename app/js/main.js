(function(){
	var doc = $(document),
		win = $(window),
		header = $('[data-id="header"]'),
		form = $('[data-validate="form"] form'),
		headerHeight = header.outerHeight(),
		learnSteps = {
			items: 1,
			nav: false,
			dots: true,
			dotsContainer: '[data-slider-nav="learn-steps"]',
			autoplay: false,
			animateIn: 'fadeInRight',
			animateOut: 'fadeOutLeft',
			lazyLoad: true,
			mouseDrag: false,
			touchDrag: false,
			pullDrag: false,
			freeDrag: false
		},
		quizSteps = {
			items: 1,
			nav: false,
			dots: true,
			dotsContainer: '[data-slider-nav="quiz-steps"]',
			autoplay: false,
			autoHeight: true,
			mouseDrag: false,
			touchDrag: false,
			pullDrag: false,
			freeDrag: false
		},
		videoSlider = {
			items: 1,
			margin: 50,
			autoplay: false,
			nav: false,
			dots: false,
			responsive: {
				992: {
					items: 2,
					margin: 40
				},
				1200: {
					items: 3,
					margin: 17
				}
			}
		},
		validateOptions = {
			errorPlacement: function(error, element) {
				error.appendTo(element.parents('.form-group'));
			},
			submitHandler: function(form) {
				$('#modalThanks').modal('show');
				$('#modalOffer').modal('hide');
				setTimeout(function() {
					form.submit();
				}, 3000);
			}
		},
		scrollT;
		$.validator.addMethod("valueNotEquals", function(value, element, arg){
			return arg !== value;
		}, function(params, element) {
			return $(element).data('msg-required');
		});


	var waitForFinalEvent = (function () {
		var timers = {};
		return function (callback, ms, uniqueId) {
			if (!uniqueId) {
				uniqueId = "Don't call this twice without a uniqueId";
			}
			if (timers[uniqueId]) {
				clearTimeout (timers[uniqueId]);
			}
			timers[uniqueId] = setTimeout(callback, ms);
		};
	})();

	function calcValues() {
		headerHeight = header.outerHeight();
	};

	function checkboxSubmit() {
		if ( $(this).is(':not(:checked)')) {
			$(this).siblings().find('input[type="submit"]').prop('disabled', true)
		} else {
			$(this).siblings().find('input[type="submit"]').prop('disabled', false)
		}
	};

	function menuOpen() {
		var dataAttr = $(this).attr('data-open');
		$('body').addClass('scroll-hidden')
		$('[data-id="' + dataAttr + '"]').addClass('visible');
	};

	function menuClose() {
		var dataAttr = $(this).attr('data-close');
		$('body').removeClass('scroll-hidden');
		$('[data-id="' + dataAttr + '"]').removeClass('visible');
	};

	function headerOnScroll() {
		if ( scrollT > 0 ) {
			header.addClass('header_fixed');
		} else {
			// calcValues();
			// setIndentsAfterHeader();
		}
		if (scrollT < 50) {
			header.removeClass('header_fixed');
		}
	};

	function setIndentsAfterHeader() {
		var content = $('[data-id="header-indents"]').css({'padding-top': headerHeight})
	};

	function scrollSpyAnchorAnimate(event) {
		if (this.hash !== "") {
			header.addClass('header_fixed');
			event.preventDefault();
			var hash = this.hash;
			headerHeight = header.outerHeight();
			$('html, body').animate({
				scrollTop: $(hash).offset().top
			}, 500);
		}
	};

	function nextSliderStep(elem, owl) {
		$(elem + ' li').click(function () {
			$(owl).trigger('to.owl.carousel', [$(this).index(), 1000]);
		});
	};
	function nextSliderStepButton(elem, owl) {
		$(elem).click(function () {
			var formGroup = $(this).closest('.quiz__step');
			var flag = false;
			var totalFlag;
			var arrFlag = [];
			formGroup.find('.form-control, select, input[type="radio"]').each(function(i, elem){
				// 
				if ( $(elem).valid() ) {
					flag = true;
					arrFlag.push('true');
				} else {
					flag = false;
					arrFlag.push('false');
				}
			});
			flag =  arrFlag.indexOf( 'false' ) != -1;
			if ( !flag ) {
				$(owl).trigger('next.owl.carousel');
			}
		});
	};
	function prevSliderStepButton(elem, owl) {
		$(elem).click(function () {
			$(owl).trigger('prev.owl.carousel');
		});
	};

	function animatedNumberCounter(elem) {
		$(elem).each(animatedNumberCounter);
		$(this).parent().parent().addClass('visible')
		$(this).prop('Counter',0).animate({
			Counter: $(this).text()
		}, {
			duration: 3000,
			easing: 'swing',
			step: function (now) {
				$(this).text(Math.ceil(now));
			}
		});
	};
	function playPause() {
		
		$(this).find('.video-box__video').get(0).play();
		$(this).find('.video-box__poster').toggle();
		$(this).find('.video-box__button').toggle();
		$(this).parent().find('.video-box__poster-title').toggle();
	};

	function closeMenuOnClickAnchor() {
		$('.nav-pills .nav-link').click(function() {
			$('.header__menu').removeClass('visible');
			$('body').removeClass('scroll-hidden');
		})
	}

	doc.ready(function() {
		svg4everybody({
			loadSprite: false
		});
		closeMenuOnClickAnchor();
		var $someImages = $('img.fit-image');
		// objectFitImages($someImages);
		animatedNumberCounter('[data-count-text]');
		$('[data-checkbox]').on('change', checkboxSubmit);
		$('[data-checkbox]').each(function() {
			if ( $(this).is(':not(:checked)')) {
				$(this).siblings().find('input[type="submit"]').prop('disabled', true)
			} else {
				$(this).siblings().find('input[type="submit"]').prop('disabled', false)
			}
		});
		$('[data-open]').click(menuOpen);
		$('[data-close]').click(menuClose);
		$('[data-video]').click(playPause);
		nextSliderStepButton('[data-step="next"]:not([disabled])', '[data-slider="quiz-steps"]');
		prevSliderStepButton('[data-step="back"]', '[data-slider="quiz-steps"]');
		$('[data-validate="form"] form').each(function() {
			$(this).validate(validateOptions);
		})
		$("#menu-scrolSpy a").click(scrollSpyAnchorAnimate);
		if ( $('[data-slider="learn-steps"]').length ) {
			if ( win.width() >= (992-17) ) {
				$('[data-slider="learn-steps"]').owlCarousel(learnSteps);
				nextSliderStep('[data-slider-nav="learn-steps"]', '[data-slider="learn-steps"]');
			}
		}
		if ( $('[data-slider="quiz-steps"]').length ) {
			$('[data-slider="quiz-steps"]').owlCarousel(quizSteps);
		}
		if ( $('[data-slider="video-cards"]').length ) {
			$('[data-slider="video-cards"]').owlCarousel(videoSlider);
		}
		$('#offer-img').addClass('visible');
		if ( $('#typed').length ) {
			var typed = new Typed('#typed', {
				stringsElement: '#typed-strings',
				typeSpeed: 50,
				backSpeed: 50,
				backDelay: 1000,
				loop: true,
				loopCount: Infinity
			})
		};
		$('.collapse').on('show.bs.collapse', function() {
			var imgs = $(this).find('.lazy-img');
			var backgrounds = $(this).find('.lazy-background');
			imgs.each(function() {
				$(this).addClass('lazyload')
			});
			backgrounds.each(function() {
				$(this).addClass('lazyload')
			})
		});
		$('[data-toggle="tab"]').on('show.bs.tab', function() {
			var href = $(this).attr('href');
			var imgs = $(href).find('.lazy-img');
			imgs.each(function() {
				if ( !$(this).closest('.collapse').length ) {
					$(this).addClass('lazyload')
				}
			})
		})
		$('.modal').on('show.bs.modal', function() {
			var imgs = $(this).find('.lazy-img');
			imgs.each(function() {
				$(this).addClass('lazyload')
			})
		});
	});
	// function loadCourse() {
	// 	var elem = document.createElement('script');
	// 	elem.type = 'text/javascript';
	// 	elem.id = '5787d815b7aa2e6e25bd15d909490f1d34bceb5a';
	// 	elem.src = 'http://kurs.nbe.bz/pl/lite/widget/script?id=77144';
	// 	document.querySelector('.form-group_mailing').appendChild(elem);
	// };
	// var checkVar = true;
	// $(window).on('scroll', function() {
	// 	var top_of_element = $(".footer").offset().top;
	// 	var bottom_of_element = $(".footer").offset().top + $(".footer").outerHeight();
	// 	var bottom_of_screen = $(window).scrollTop() + $(window).outerHeight();
	// 	var top_of_screen = $(window).scrollTop();

	// 	if ( checkVar ) {
	// 		if ((bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element)){
	// 			// loadCourse();
	// 			checkVar = false;
	// 		}
	// 	}
	// });
	win.on('scroll resize load', function(e) {
		if ( e.type == 'scroll' ) {
			scrollT = $(this).scrollTop();
		}
		if ( e.type == 'resize' ) {
			waitForFinalEvent(function() {
				if ( $('[data-slider="learn-steps"]').length ) {
					if ( win.width() < (992-17) ) {
						$('[data-slider="learn-steps"]').trigger('destroy.owl.carousel');
						$('[data-slider="learn-steps"]').removeClass('owl-carouse owl-theme');
					} else {
						$('[data-slider="learn-steps"]').addClass('owl-carousel owl-theme');
						$('[data-slider="learn-steps"]').owlCarousel(learnSteps);
						nextSliderStep('[data-slider-nav="learn-steps"]', '[data-slider="learn-steps"]');
					}
				}
			}, 500, "resize")
		}
		if ( e.type == 'load') {
		}
	})
}())